.PHONY: all

rsses:=$(patsubst %.tsv,%.rss,$(wildcard data/*.tsv))

all: report.html


report.html: scripts/proc_rsses.R $(rsses)
	Rscript -e 'rmarkdown::render("$<",output_file="../$@")'
	mv scripts/*jpeg ./ 

data/%.rss: data/%.tsv
	wget -O data/$$(cat $< | sed "s/ .*//").rss  $$(cat $< | sed "s/.* //")
	touch $@
