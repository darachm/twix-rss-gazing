This is a little set of scripts to do text analysis of the microbe.tv RSS feeds.
It's set up with a Makefile. So, on a sane computer, you can run the whole 
analysis by typing `make all`.

You'll need to have `make` installed to do that, and `wget` to retrieve the
RSS feeds. You may just want the R, which is inside the `scripts` folder
and requires the libraries `xst`, `XML`, and the `tidyverse` metapackage.

We welcome folks raising issues (see left menu) or submitting pull requests.
Yep.
